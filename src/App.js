import React, { Component } from 'react';
import logo from './FreedrumLogoBlack.png';
import './App.css';
import Horizontal from './valueSlider'
import { presets, preset10zones,factoryPreset } from './presets'


class Square extends React.Component {
  render() {
    return (
      <button className="square" style={{ height: "50px", width: "50px" }} >
        {this.props.midiNote} {this.props.value ? "*" : ""}
      </button>
    );
  }
}
var handlePadChange;
var theboard;

class Board extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      currentPad: 1,
      preset: currentPreset
    };
    theboard = this
    handlePadChange = (pad) => {

      this.setState({
        currentPad: pad
      })
    }
  }
  renderSquare(i) {
    return (<Square midiNote={this.state.preset.zones.length > i ? this.state.preset.zones[i].midiNote : -1} value={this.state.currentPad === i} />);
  }


  render() {
    const status = '';

    return (
      <div>
        <div className="status">{status}</div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
          {this.renderSquare(7)}
        </div>
        <div className="board-row">
          {this.renderSquare(8)}
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
          {this.renderSquare(9)}

        </div>

      </div>
    );
  }
}

function logText(str) {
  console.log(str)
  // log.innerHTML += str;
  // log.innerHTML += "<br>";
  // log.scrollTop = log.scrollHeight;
}
var MidiConstants = {
  STATUS_CONTROL_CHANGE: 0xB0
}
class Sensor {
  constructor(midi) {
    this.midiOut = midi

  }

  outstandingWrites = 0
  writeCC(channel, cc, value) {

    this.outstandingWrites++
    setTimeout(() => {
      var data = [MidiConstants.STATUS_CONTROL_CHANGE | channel, cc, value]
      this.midiOut.send(data)
      console.log("cc" + cc)
      this.outstandingWrites--
    }, 100 * this.outstandingWrites)

  }

  readSetting(cc){
    this.writeCC(0,22,cc)
  }
  setNote(zone, note) {
    console.log("zone" + zone + " note:" + note)
    this.writeCC(zone, 106, note)

  }
  sendPadPos(padNr, y, z) {

    var angleScale = 360.0 / 127;
    function scaleAngle(x) {
      if (x >= 0) return (x / angleScale)
      else return 127 + (x / angleScale)
    }

    var y_pos = 23
    var z_pos = 103
    this.writeCC(padNr, y_pos, scaleAngle(y))
    this.writeCC(padNr, z_pos, scaleAngle(z))

  }
  sendPreset(preset) {
    var zones = preset.zones
    for (var i = 0; i < 10; i++){
      if (i < zones.length) {
        var s = zones[i]
        this.setNote(i, s.midiNote)
        if (s.z != null && s.y != null)
          this.sendPadPos(i, s.y, s.z)
      }
      else
        this.setNote(i, 0)
    }
    this.saveSettings()
  }
  freedrumCommandCC=20
  factoryReset(){
    const factoryResetCmd=1;
    
    this.writeCC(0,this.freedrumCommandCC,factoryResetCmd);
  }

  saveSettings(){
    const saveCmd=0;    
    this.writeCC(0,this.freedrumCommandCC,saveCmd);
  }
}
var currentSensor = null
var currentPreset = preset10zones
class PresetList extends Component {

  sendPreset(zones) {
    currentSensor.sendPreset(zones)
    currentPreset = zones
    theboard.setState({
      preset: zones
    })
  }
  render() {

    return (
      <div>
        <div>
          {presets.map((p) => {
            return (
              
                <button className="PresetButton" onClick={() => { this.sendPreset(p) }}>
                  {p.name}
                </button>
              )
          })}

        <div>
        <button className="PresetButton" onClick={() => { currentSensor.factoryReset();
          theboard.setState({
            preset: factoryPreset
          })
        }}>
        Factory reset
        </button>
        </div>
        </div>
      </div>
    )
  }
}

var controllerCallback=null

class MidiDeviceList extends Component {

  onFailure() {

  }
  constructor() {
    super()

    this.state = {
      outputs: [{ id: 3, name: "dsdf" }],
      currentOutput: 0
    }

    logText("Initializing MIDI...");
    navigator.requestMIDIAccess().then((midi) => { this.onSuccess(midi) }, this.onFailure); //get midi access
  }
  o = []
  onSuccess(midi) {
    var outputs = midi.outputs;

    logText("Found " + outputs.size + " MIDI outputs(s)");

    //connect to first device found
    outputs.forEach(val => {
      this.o.push({
        name: val.name,
        id: val.id,
        device: val
      });
    });
    midi.inputs.forEach(inp => {
      var id = inp.id

      inp.onmidimessage = (event) => {
        if (event.data.length === 3) {
          switch (event.data[0]&0xf0){
            case 0xb0: {
              var cc = event.data[1];
              const val = event.data[2];
              logText('controller id: ' + cc + ', value: ' + val);
              if (cc === 16) {
                handlePadChange(val)
              }


              controllerCallback(cc, val)
            }          
            break

            case 0x90:{
              const note= event.data[1];
              const vel= event.data[2];
              logText(`Note on:${note}, vel:${vel}`)
            }
        }
      }
      }
    });

    this.setState({
      outputs: this.o
    })

    if (outputs.size > 0) {
      var iterator = outputs.values();
      currentSensor = new Sensor(iterator.next().value)
    }
    // if(inputs.size > 0) {
    // 	var iterator = inputs.values(); // returns an iterator that loops over all inputs
    // 	var input = iterator.next().value; // get the first input
    // 	logText("Connected first input: " + input.name);
    // 	//input.onmidimessage = handleMIDIMessage;
    // }
  }



  onChange(val) {
    logText(val)

    var dev = this.o[val]
    currentSensor = new Sensor(dev.device)
  }
  render() {
    var midiOptions = this.state.outputs.map(element => {
      return (<option id={element.id}>
        {element.name}
      </option>)
    })



    return (
      <div>
        <select
          onChange={(sel) => { this.onChange(sel.target.selectedIndex) }} >
          {midiOptions}
        </select>

      </div>
    );
  }
}
class App extends Component {

  constructor() {
    super()

    this.state = {
      controllerValues:[
        {cc:104,val:1,name:"Threshold"},{cc:24,val:0,name:"Battery"},
        {cc:105,name:"Sensor sensitivity"},
        {cc:111,name:"Ref drum strength"},
        {cc:116,name:"Trigger mask time"}
      ]
    }

    controllerCallback=(cc,val)=>{
      var cv=this.state.controllerValues.map(cobj => {if (cobj.cc===cc) {
        cobj.val=val; return cobj;}
         else return cobj;} )
      this.setState({
        controllerValues:cv
      })
    }
  }

  controllerLine=(c,index)=>{
    return (
    <Horizontal controllerId={c.cc} value={this.state.controllerValues[index].val} 
    onControllerChange={(cc,val) => {
      c.val=val
      var cv=this.state.controllerValues.slice()

      //cv[cc]=val
      this.setState({
        controllerValues:cv
      })
    }
    }
    onControllerComplete={()=>currentSensor.writeCC(0,c.cc,this.state.controllerValues[index].val)}
    onRead={()=>currentSensor.readSetting(c.cc)}
    label={this.state.controllerValues[index].name}
   />)        
    };
  advancedSettings=()=>{
    return <div style={{width:"600px",margin:"auto"}}>
        <p>Settings</p>
        {
         this.state.controllerValues.map(this.controllerLine)         
        }
        </div>
  }
    
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
          <text style={{textAlign:"right"}} className="App-title">Sensor Settings Test Platform</text>
          </p>
        </header>
        <div style={{textAlign:"center"}}> 
        <p>Select sensor</p>
        <MidiDeviceList />
        <p>Send preset</p>
        <PresetList />
        <p>Zone Mapping Guide:</p>
        <Board />
        Show advanced <input type="checkbox" defaultChecked={this.state.showAdvanced} onChange={()=>{this.setState({showAdvanced:!this.state.showAdvanced})}}/>
        {
          this.state.showAdvanced?this.advancedSettings():""
        }
        
        </div>
        
      </div>
    );
  }
}

export default App;
