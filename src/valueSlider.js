import React, { Component } from 'react'
import Slider from 'react-rangeslider'
// To include the default styles
import 'react-rangeslider/lib/index.css'
class Horizontal extends Component {
  constructor (props, context) {
    super(props, context)
  }

  handleChangeStart = () => {
    console.log('Change event started')
  };

  handleChange = value => {
    
    //send controller
    console.log("controller:"+this.props.controllerId)
    this.props.onControllerChange(this.props.controllerId,value)
  };

  handleChangeComplete = () => {
    console.log('Change event completed')
    this.props.onControllerComplete()
  };

  render () {
    return (
      <div className='slider' style={{paddingLeft:"100px",paddingRight:"100px",marginBottom:"50px"}}>
      
        <Slider
          min={0}
          max={100}
          value={this.props.value}
          onChangeStart={this.handleChangeStart}
          onChange={this.handleChange}
          onChangeComplete={this.handleChangeComplete}
        />
       <div style={{display:"flex"}}>
        <button onClick={this.props.onRead} style={{marginRight:"50px"}}>Read</button>
        {this.props.label}: <div  style={{marginLeft:"20px"}}className='value'>{this.props.value}</div>
        
        </div>
      </div>
    )
  }
}

export default Horizontal